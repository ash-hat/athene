using System;
using System.Collections.Generic;
using Athene.Components.Interpolators;

namespace Athene.Tests.Interpolation
{
	public class DummyEscapeSequence : EscapeSequence<IExecutionContext>
	{
		public DummyEscapeSequence(IEnumerable<char> startSequence) : base(startSequence)
		{
		}

		protected override IEnumerable<char> Escape(IEnumerator<char> raw, IExecutionContext context)
		{
			throw new NotImplementedException();
		}
	}
}
