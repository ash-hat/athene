using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Components.Interpolators;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class DynamicUnicodeEscapeSequenceTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new DynamicUnicodeEscapeSequence<IExecutionContext>(new char[0]);
		}

		[Fact]
		public void Constructor_NullStartSequence_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new DynamicUnicodeEscapeSequence<IExecutionContext>(null));
		}

		[Theory]
		[InlineData("\\x", "\\x1 and \\x1000", "\x1 and \x1000")]
		[InlineData("\\x", "\\x01 and \\x100", "\x01 and \x100")]
		[InlineData("\\x", "\\x001 and \\x10", "\x001 and \x10")]
		[InlineData("AAA", "ABAABBAAA0001BBB", "ABAABB\x0001BBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ue0001f893 8nfu8943", "weuri 4rw8lg8o\x0001f893 8nfu8943")]
		public void Escape_Clean_Equal(string startSequence, string raw, string expected)
		{
			DynamicUnicodeEscapeSequence<IExecutionContext> sequence = new DynamicUnicodeEscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Equal(expected.ToCharArray(), sequence.Interpolate(raw.ToCharArray(), null));
		}

		[Theory]
		[InlineData("\\x", "\\xabc and \\xZ")]
		[InlineData("AAA", "ABAABBAAAXBADBBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ueHAHAf893 8nfu8943")]
		public void Escape_InvalidHex_ArgumentException(string startSequence, string raw)
		{
			DynamicUnicodeEscapeSequence<IExecutionContext> sequence = new DynamicUnicodeEscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Throws<ArgumentException>(() => sequence.Interpolate(raw.ToCharArray(), null).ToList());
		}
	}
}
