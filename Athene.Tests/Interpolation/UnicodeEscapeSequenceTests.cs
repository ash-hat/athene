using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Components.Interpolators;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class UnicodeEscapeSequenceTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new UnicodeEscapeSequence<IExecutionContext>(new char[0]);
		}

		[Fact]
		public void Constructor_NullStartSequence_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new UnicodeEscapeSequence<IExecutionContext>(null));
		}

		[Theory]
		[InlineData("\\u", "\\u0001 and \\u1000", "\u0001 and \u1000")]
		[InlineData("AAA", "ABAABBAAA0001BBB", "ABAABB\u0001BBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ue0001f893 8nfu8943", "weuri 4rw8lg8o\u0001f893 8nfu8943")]
		public void Escape_Clean_Equal(string startSequence, string raw, string expected)
		{
			UnicodeEscapeSequence<IExecutionContext> sequence = new UnicodeEscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Equal(expected.ToCharArray(), sequence.Interpolate(raw.ToCharArray(), null));
		}

		[Theory]
		[InlineData("\\u", "\\uabcd and \\uxbad")]
		[InlineData("AAA", "ABAABBAAAXBADBBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ueHAHAf893 8nfu8943")]
		public void Escape_InvalidHex_ArgumentException(string startSequence, string raw)
		{
			UnicodeEscapeSequence<IExecutionContext> sequence = new UnicodeEscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Throws<ArgumentException>(() => sequence.Interpolate(raw.ToCharArray(), null).ToList());
		}
	}
}
