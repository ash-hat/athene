using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class FooBarInterpolatorTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new IdentityInterpolator();
		}

		[Fact]
		public void Escape_Clean_Equal()
		{
			char[] charSequence = "3948o vtlb ib9oqpn932olv vth7n9l4z.w035nbgh8 tsikxeltgm,rfd ".ToCharArray();
			IdentityInterpolator interpolator = new IdentityInterpolator();

			Assert.Equal(charSequence, interpolator.Interpolate(new ArraySegment<char>(charSequence), default));
		}
	}
}
