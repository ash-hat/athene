using System.Collections.Generic;
using Athene.Components.Interpolators;

namespace Athene.Tests.Interpolation
{
	public class IdentityInterpolator : IInterpolator<IExecutionContext>
	{
		public IEnumerable<char> Interpolate(IEnumerable<char> raw, IExecutionContext context)
		{
			return raw;
		}
	}
}
