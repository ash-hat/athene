using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Athene.Components.Interpolators;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class AggregateInterpolationTests
	{
		[Fact]
		public void Constructor()
		{
			AggregateInterpolator<IExecutionContext> interpolator = new AggregateInterpolator<IExecutionContext>();

			Assert.Same(AggregateInterpolator<IExecutionContext>.DefaultEscapeCharacters, interpolator.Interpolators);
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_NonNullInterpolators_Pass()
		{
			new AggregateInterpolator<IExecutionContext>(new IInterpolator<IExecutionContext>[0]);
		}

		[Fact]
		public void Constructor_NullInterpolators_AggregateNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AggregateInterpolator<IExecutionContext>(null));
		}

		[Fact]
		public void DefaultEscapeCharactersGetter_Equal()
		{
			Assert.Same(AggregateInterpolator<IExecutionContext>.DefaultEscapeCharacters, AggregateInterpolator<IExecutionContext>.DefaultEscapeCharacters);
		}

		[Fact]
		public void InterpolatorsGetter_Clean_Equal()
		{
			IEnumerable<IInterpolator<IExecutionContext>> interpolators = AggregateInterpolator<IExecutionContext>.DefaultEscapeCharacters;
			AggregateInterpolator<IExecutionContext> interpolator = new AggregateInterpolator<IExecutionContext>(interpolators);

			Assert.Same(interpolators, interpolator.Interpolators);
		}

		[Fact]
		public void InterpolatorsSetter_Clean_Equal()
		{
			IEnumerable<IInterpolator<IExecutionContext>> interpolators = AggregateInterpolator<IExecutionContext>.DefaultEscapeCharacters;
			AggregateInterpolator<IExecutionContext> interpolator = new AggregateInterpolator<IExecutionContext>(new IInterpolator<IExecutionContext>[0]);

			Assert.NotSame(interpolators, interpolator.Interpolators);

			interpolator.Interpolators = interpolators;

			Assert.Same(interpolators, interpolator.Interpolators);
		}

		[Fact]
		public void Interpolate_Clean_Equal()
		{
			char[] raw = "asfhiuseyuikfg ykzusdkugewriog9032 7t98238ov nr".ToCharArray();
			AggregateInterpolator<IExecutionContext> interpolator = new AggregateInterpolator<IExecutionContext>();

			Assert.Equal(raw, interpolator.Interpolate(raw, null));
		}
	}
}
