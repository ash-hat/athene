using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Components.Interpolators;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class Utf32EscapeSequenceTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new Utf32EscapeSequence<IExecutionContext>(new char[0]);
		}

		[Fact]
		public void Constructor_NullStartSequence_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Utf32EscapeSequence<IExecutionContext>(null));
		}

		[Theory]
		[InlineData("\\U", "\\U00000010 and \\U00000100", "\U00000010 and \U00000100")]
		[InlineData("AAA", "ABAABBAAA00000001BBB", "ABAABB\U00000001BBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ue00000001f893 8nfu8943", "weuri 4rw8lg8o\U00000001f893 8nfu8943")]
		public void Escape_Clean_Equal(string startSequence, string raw, string expected)
		{
			Utf32EscapeSequence<IExecutionContext> sequence = new Utf32EscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Equal(expected.ToCharArray(), sequence.Interpolate(raw.ToCharArray(), null));
		}

		[Theory]
		[InlineData("\\U", "\\Uabcd0000 and \\Uxbad0000")]
		[InlineData("AAA", "ABAABBAAAXBAD0000BBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89ueHAHA0000f893 8nfu8943")]
		public void Escape_InvalidHex_ArgumentException(string startSequence, string raw)
		{
			Utf32EscapeSequence<IExecutionContext> sequence = new Utf32EscapeSequence<IExecutionContext>(startSequence.ToCharArray());

			Assert.Throws<ArgumentException>(() => sequence.Interpolate(raw.ToCharArray(), null).ToList());
		}
	}
}
