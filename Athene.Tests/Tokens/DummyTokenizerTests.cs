using System;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class DummyTokenizerTests
	{
		[Fact]
		public void Tokenize_Any_NotImplemented()
		{
			DummyTokenizer tokenizer = new DummyTokenizer();

			Assert.Throws<NotImplementedException>(() => tokenizer.Tokenize(null, default));
		}
	}
}
