using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class RawDelemitedClosureTests
	{
		[Theory]
		[InlineData("abc")]
		[InlineData("a")]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor(string delimiter)
		{
			new RawDelimitedClosure<IExecutionContext>(delimiter);
		}

		[Fact]
		public void Constructor_NullDelimiter_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawDelimitedClosure<IExecutionContext>(null));
		}

		[Fact]
		public void Constructor_EmptyDelimiter_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new RawDelimitedClosure<IExecutionContext>(string.Empty));
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void Interpolate(bool interpolate)
		{
			RawDelimitedClosure<IExecutionContext> closure = new RawDelimitedClosure<IExecutionContext>("aaa");

			Assert.False(closure.Interpolate);

			closure.Interpolate = interpolate;
			Assert.Equal(interpolate, closure.Interpolate);
		}

		[Theory]
		[InlineData('a')]
		[InlineData('b')]
		[InlineData('\0')]
		[InlineData(null)]
		public void EscapeCharacter(char? escapeCharacter)
		{
			RawDelimitedClosure<IExecutionContext> closure = new RawDelimitedClosure<IExecutionContext>("aaa");

			Assert.Null(closure.EscapeCharacter);

			closure.EscapeCharacter = escapeCharacter;
			Assert.Equal(escapeCharacter, closure.EscapeCharacter);
		}

		[Theory]
		[InlineData("aeiou")]
		[InlineData("")]
		public void Process(string token)
		{
			RawDelimitedClosure<IExecutionContext> closure = new RawDelimitedClosure<IExecutionContext>("aaa");

			Assert.Equal(token.ToCharArray(), closure.Process(token, null).Value);
		}

		[Fact]
		public void Process_NullToken_ArgumentNullException()
		{
			RawDelimitedClosure<IExecutionContext> closure = new RawDelimitedClosure<IExecutionContext>("aaa");

			Assert.Throws<ArgumentNullException>(() => closure.Process(null, null));
		}

		[Fact]
		public void Dispose()
		{
			RawDelimitedClosure<IExecutionContext> closure = new RawDelimitedClosure<IExecutionContext>("aaa");

			closure.Dispose();

			Assert.Throws<ObjectDisposedException>(() => closure.Interpolate);
			Assert.Throws<ObjectDisposedException>(() => closure.AdvanceDelimiter(default, null, out _));
			Assert.Throws<ObjectDisposedException>(() => closure.Process(null, null));
		}
	}
}
