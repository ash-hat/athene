using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class RawWrappedClosureTests
	{
		[Theory]
		[InlineData("abc", "cba")]
		[InlineData("a", "a")]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor(string opening, string closing)
		{
			new RawWrappedClosure<IExecutionContext>(opening, closing);
		}

		[Fact]
		public void Constructor_NullOpening_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawWrappedClosure<IExecutionContext>(null, "aaa"));
		}

		[Fact]
		public void Constructor_NullClosing_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawWrappedClosure<IExecutionContext>("aaa", null));
		}

		[Fact]
		public void Constructor_EmptyOpening_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new RawWrappedClosure<IExecutionContext>(string.Empty, "aaa"));
		}

		[Fact]
		public void Constructor_EmptyClosing_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new RawWrappedClosure<IExecutionContext>("aaa", string.Empty));
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void Interpolate(bool interpolate)
		{
			RawWrappedClosure<IExecutionContext> closure = new RawWrappedClosure<IExecutionContext>("aaa", "aaa");

			Assert.False(closure.Interpolate);

			closure.Interpolate = interpolate;
			Assert.Equal(interpolate, closure.Interpolate);
		}

		[Theory]
		[InlineData('a')]
		[InlineData('b')]
		[InlineData('\0')]
		[InlineData(null)]
		public void EscapeCharacter(char? escapeCharacter)
		{
			RawWrappedClosure<IExecutionContext> closure = new RawWrappedClosure<IExecutionContext>("aaa", "aaa");

			Assert.Null(closure.EscapeCharacter);

			closure.EscapeCharacter = escapeCharacter;
			Assert.Equal(escapeCharacter, closure.EscapeCharacter);
		}

		[Theory]
		[InlineData("aeiou")]
		[InlineData("")]
		public void Process(string token)
		{
			RawWrappedClosure<IExecutionContext> closure = new RawWrappedClosure<IExecutionContext>("aaa", "aaa");

			Assert.Equal(token.ToCharArray(), closure.Process(token, null).Value);
		}

		[Fact]
		public void Process_NullToken_ArgumentNullException()
		{
			RawWrappedClosure<IExecutionContext> closure = new RawWrappedClosure<IExecutionContext>("aaa", "aaa");

			Assert.Throws<ArgumentNullException>(() => closure.Process(null, null));
		}

		[Fact]
		public void Dispose()
		{
			RawWrappedClosure<IExecutionContext> closure = new RawWrappedClosure<IExecutionContext>("aaa", "aaa");

			closure.Dispose();

			Assert.Throws<ObjectDisposedException>(() => closure.Interpolate);
			Assert.Throws<ObjectDisposedException>(() => closure.EscapeCharacter);
			Assert.Throws<ObjectDisposedException>(() => closure.EscapeCharacter = null);
			Assert.Throws<ObjectDisposedException>(() => closure.AdvanceOpening(default, null, out _));
			Assert.Throws<ObjectDisposedException>(() => closure.AdvanceClosing(default, null, out _));
			Assert.Throws<ObjectDisposedException>(() => closure.Process(null, null));
		}
	}
}
