using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Tables;
using Xunit;

namespace Athene.Tests.Commands.Control
{
	public class SwitchCommandTests
	{
		private const string DummyDescription = "Dummy";

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new SwitchCommand<IExecutionContext>(DummyDescription);
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_NullDescription_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new SwitchCommand<IExecutionContext>(null));
		}

		[Fact]
		public void ChildCommandsGetter_Clean_NotNull()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.NotNull(command.Children);
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void ChildCommandsSetter_Clean_Pass()
		{
			new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>()
			};
		}

		[Fact]
		public void ChildCommandsSetter_NullValue_ArgumentNullException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<ArgumentNullException>(() => command.Children = null);
		}

		[Fact]
		public void DefaultChildCommandGetter_Clean_Null()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Null(command.DefaultChild);
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void DefaultChildCommandSetter_Clean_Pass()
		{
			new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				DefaultChild = new DummyCommand()
			};
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void DefaultChildCommandSetter_NullValue_Pass()
		{
			new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				DefaultChild = null
			};
		}

		[Fact]
		public void Execute_OneCommand_Equal()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(FooBarCommand))] = new FooBarCommand()
				}
			};

			Assert.Equal(FooBarCommand.Output, command.Execute(new[] { nameof(FooBarCommand) }.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_IndexOutOfRange_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<CommandException>(() => command.Execute(new string[0].AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_IndexOutOfRangeWithDefault_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				DefaultChild = new FooBarCommand()
			};

			Assert.Equal(FooBarCommand.Output, command.Execute(new string[0].AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_Help_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				DefaultChild = new DummyCommand()
			};

			Assert.NotNull(command.Execute(new[]
			{
				"--help"
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_HelpOneCommand_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(DummyCommand))] = new DummyCommand()
				}
			};

			Assert.NotNull(command.Execute(new[]
			{
				"--help",
				nameof(DummyCommand)
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_HelpNestedSwitchCommand_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(SwitchCommand<IExecutionContext>))] = new SwitchCommand<IExecutionContext>(DummyDescription)
					{
						Children = new AliasTable<ICommand<IExecutionContext>>
						{
							[new Aliases(nameof(DummyCommand))] = new DummyCommand()
						},
						DefaultChild = new DummyCommand()
					}
				}
			};

			Assert.NotNull(command.Execute(new[]
			{
				"--help",
				nameof(SwitchCommand<IExecutionContext>)
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_HelpNoDefault_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<CommandException>(() => command.Execute(new[]
			{
				"--help"
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_InvalidMode_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(DummyCommand))] = new DummyCommand()
				}
			};

			Assert.Throws<CommandException>(() => command.Execute(new[]
			{
				nameof(DummyCommand).GetHashCode().ToString()
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_EmptyTokens_CommandException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<CommandException>(() => command.Execute(new string[0].AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_NullTokens_ArgumentNullException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<ArgumentNullException>(() => command.Execute(null, null));
		}

		[Fact]
		public void Execute_NullToken_ArgumentNullException()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription);

			Assert.Throws<ArgumentException>(() => command.Execute(new string[]
			{
				null
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void DescriptionGetter_OneCommand_NotNull()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(DummyCommand))] = new DummyCommand()
				}
			};

			Assert.NotNull(command.Description);
		}

		[Fact]
		public void SignatureGetter_OneCommand_NotNull()
		{
			SwitchCommand<IExecutionContext> command = new SwitchCommand<IExecutionContext>(DummyDescription)
			{
				Children = new AliasTable<ICommand<IExecutionContext>>
				{
					[new Aliases(nameof(DummyCommand))] = new DummyCommand()
				}
			};

			Assert.NotNull(command.Signature);
		}
	}
}
