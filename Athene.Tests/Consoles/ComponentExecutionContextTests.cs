﻿using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components;
using Athene.Components.Contexts;
using Athene.Tables;
using Athene.Tests.Commands;
using Athene.Tests.Tokens;
using Xunit;

namespace Athene.Tests.Consoles
{
	public class ComponentExecutionContextTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>(), new DummyTokenizer());
		}

		[Fact]
		public void Constructor_NullCommands_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ComponentExecutionContext(null, new DummyTokenizer()));
		}

		[Fact]
		public void Constructor_NullTokenizer_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>(), null));
		}

		[Fact]
		public void Execute_NoCommands_NotFound()
		{
			IExecutionContext context = new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>(), new IdentityTokenizer());

			Assert.Equal(ResultOutcome.NotFound, context.Execute("notfound").Outcome);
		}

		[Fact]
		public void Execute_OneCommand_Success()
		{
			const string alias = "shouldbefound";
			IExecutionContext context = new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>
			{
				[new Aliases(alias)] = new FooBarCommand()
			}, new IdentityTokenizer());
			CommandResult result = context.Execute(alias);

			Assert.Equal(ResultOutcome.Success, result.Outcome);
			Assert.Equal(FooBarCommand.Output, result.Message);
		}

		[Fact]
		public void Execute_OneDummyCommand_Fail()
		{
			const string alias = "shouldbefound";
			IExecutionContext context = new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>
			{
				[new Aliases(alias)] = new DummyCommand()
			}, new IdentityTokenizer());
			CommandResult result = context.Execute(alias);

			Assert.Equal(ResultOutcome.Fail, result.Outcome);
		}
	}
}
