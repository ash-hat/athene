using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Parameters.Arguments;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class RawArgumentTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new RawArgument("example", "raw", "Example argument.");
		}

		[Fact]
		public void Constructor_NullName_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawArgument(null, "raw", "Example argument."));
		}

		[Fact]
		public void Constructor_NullValueName_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawArgument("example", null, "Example argument."));
		}

		[Fact]
		public void Constructor_NullDescription_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawArgument("example", "raw", null));
		}

		[Fact]
		public void ProcessValue_Clean_Equal()
		{
			const string input = "Example";
			RawArgument argument = new RawArgument("example", "raw", "Example argument.");
			IEnumerator<string> enumerator = new[]
			{
				input
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.True(argument.Update(enumerator, null));
			Assert.Equal(input, argument.Value);
		}

		[Fact]
		public void ProcessValue_NullTokens_ArgumentNullException()
		{
			RawArgument argument = new RawArgument("example", "raw", "Example argument.");

			Assert.Throws<ArgumentNullException>(() => argument.Update(null, null));
		}

		[Fact]
		public void ProcessValue_NullToken_ArgumentException()
		{
			RawArgument argument = new RawArgument("example", "raw", "Example argument.");
			IEnumerator<string> enumerator = new string[]
			{
				null
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<ArgumentException>(() => argument.Update(enumerator, null));
		}
	}
}
