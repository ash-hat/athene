using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class ParameterCommandTests
	{
		[Fact]
		public void SignatureGetter_Clean_NotNull()
		{
			TestableParameterCommand command = new TestableParameterCommand();

			Assert.NotNull(command.Signature);
		}

		[Fact]
		public void DescriptionGetter_Clean_NotNull()
		{
			TestableParameterCommand command = new TestableParameterCommand();

			Assert.NotNull(command.Description);
		}

		[Fact]
		public void Execute_Clean_NotNull()
		{
			TestableParameterCommand command = new TestableParameterCommand();

			Assert.NotNull(command.Execute(new []
			{
				"-t",
				"--parse-option",
				100.ToString(),
				100.ToString()
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_BadParameter_CommandException()
		{
			TestableParameterCommand command = new TestableParameterCommand();

			Assert.Throws<CommandException>(() => command.Execute(new[]
			{
				"This parameter is bad.",
			}.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_NullTokens_ArgumentNullException()
		{
			TestableParameterCommand command = new TestableParameterCommand();

			Assert.Throws<ArgumentNullException>(() => command.Execute(null, null));
		}

		[Fact]
		public void Execute_NullToken_CommandException()
		{
			TestableParameterCommand command = new TestableParameterCommand();
			IEnumerator<string> enumerator = new string[]
			{
				null
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => command.Execute(enumerator, null));
		}

		[Fact]
		public void ProcessValue_EnumerationEnded_CommandException()
		{
			TestableParameterCommand command = new TestableParameterCommand();
			string[] tokens =
			{
				"-t",
				"--parse-option",
				100.ToString(),
				100.ToString()
			};
			IEnumerator<string> enumerator = tokens.AsEnumerable().GetEnumerator();

			for (int i = 0; i < tokens.Length; ++i)
			{
				enumerator.MoveNext();
			}

			Assert.Throws<CommandException>(() => command.Execute(enumerator, null));
		}
	}
}
