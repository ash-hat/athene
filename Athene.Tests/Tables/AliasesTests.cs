using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Athene.Tables;
using Xunit;

namespace Athene.Tests.Tables
{
	public class AliasesTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_EmptyArray()
		{
			new Aliases("primary");
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_Array()
		{
			new Aliases("primary", "secondary", "tertiary");
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_ReadOnlyList()
		{
			new Aliases("primary", new ReadOnlyCollection<string>(new[] { "secondary", "tertiary" }));
		}

		[Fact]
		public void Constructor_ArrayNullPrimaryAlias_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Aliases(null, "secondary", "tertiary"));
		}

		[Fact]
		public void Constructor_ListNullPrimaryAlias_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Aliases(null, (IReadOnlyCollection<string>) new[] { "secondary", "tertiary" }));
		}

		[Fact]
		public void Constructor_NullArray_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Aliases("primary", null));
		}

		[Fact]
		public void Constructor_NullReadOnlyList_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Aliases("primary", (IReadOnlyCollection<string>) null));
		}

		[Fact]
		public void Constructor_NullArrayMember_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new Aliases("primary", "secondary", null));
		}

		[Fact]
		public void Constructor_NullReadOnlyListMember_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new Aliases("primary", (IReadOnlyCollection<string>) new[] { "secondary", null }));
		}

		[Fact]
		public void PrimaryAliasGetter_Clean_Equal()
		{
			const string primaryAlias = "primary";
			Aliases aliases = new Aliases(primaryAlias);

			Assert.Equal(primaryAlias, aliases.PrimaryAlias);
		}

		[Fact]
		public void SecondaryAliasesGetter_Clean_Equal()
		{
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases aliases = new Aliases("primary", secondaryAlias, tertiaryAlias);

			Assert.Equal(new[] { secondaryAlias, tertiaryAlias }, (IEnumerable<string>) aliases.SecondaryAliases);
		}

		[Fact]
		public void Equals_Aliases_Pass()
		{
			Aliases aliases = new Aliases("primary", "secondary", "tertiary");
			Assert.True(aliases.Equals(aliases));
		}

		[Fact]
		public void Equals_Object_Pass()
		{
			Aliases aliases = new Aliases("primary", "secondary", "tertiary");
			Assert.True(aliases.Equals((object) aliases));
		}

		[Fact]
		public void GetEnumeratorOfString_Clean_Pass()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases aliases = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			using (IEnumerator<string> enumerator = aliases.GetEnumerator())
			{
				Assert.True(enumerator.MoveNext());
				Assert.Equal(primaryAlias, enumerator.Current);

				Assert.True(enumerator.MoveNext());
				Assert.Equal(secondaryAlias, enumerator.Current);

				Assert.True(enumerator.MoveNext());
				Assert.Equal(tertiaryAlias, enumerator.Current);

				Assert.False(enumerator.MoveNext());
			}
		}

		[Fact]
		public void GetEnumerator_Clean_Pass()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases aliases = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			IEnumerator enumerator = ((IEnumerable) aliases).GetEnumerator();

			Assert.True(enumerator.MoveNext());
			Assert.Equal(primaryAlias, enumerator.Current);

			Assert.True(enumerator.MoveNext());
			Assert.Equal(secondaryAlias, enumerator.Current);

			Assert.True(enumerator.MoveNext());
			Assert.Equal(tertiaryAlias, enumerator.Current);

			Assert.False(enumerator.MoveNext());
		}

		[Fact]
		[SuppressMessage("ReSharper", "EqualExpressionComparison")]
		public void Equality_Reference_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

#pragma warning disable CS1718
			Assert.True(a == a);
#pragma warning restore CS1718
		}

		[Fact]
		public void Equality_Copy_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.True(a == b);
		}

		[Fact]
		public void Equality_NoSecondaries_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias);

			Assert.False(a == b);
		}

		[Fact]
		public void Equality_Null_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.False(a == null);
		}

		[Fact]
		[SuppressMessage("ReSharper", "EqualExpressionComparison")]
		public void Inequality_Reference_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

#pragma warning disable CS1718
			Assert.False(a != a);
#pragma warning restore CS1718
		}

		[Fact]
		public void Inequality_Copy_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.False(a != b);
		}

		[Fact]
		public void EqualsOfType_Reference_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.True(a.Equals(a));
		}

		[Fact]
		public void EqualsOfType_Copy_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.True(a.Equals(b));
		}

		[Fact]
		public void EqualsOfObject_Reference_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.True(a.Equals((object) a));
		}

		[Fact]
		public void EqualsOfObject_Copy_True()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.True(a.Equals((object) b));
		}

		[Fact]
		public void EqualsOfObject_NullObj_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.False(a.Equals(null));
		}

		[Fact]
		[SuppressMessage("ReSharper", "SuspiciousTypeConversion.Global")]
		public void EqualsOfObject_BadType_False()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);

			Assert.False(a.Equals("This is not the right type, at all."));
		}

		[Fact]
		public void GetHashCode_Copy_Equal()
		{
			const string primaryAlias = "primary";
			const string secondaryAlias = "secondary";
			const string tertiaryAlias = "tertiary";
			Aliases a = new Aliases(primaryAlias, secondaryAlias, tertiaryAlias);
			Aliases b = new Aliases(primaryAlias, tertiaryAlias, secondaryAlias);

			Assert.Equal(a.GetHashCode(), b.GetHashCode());
		}
	}
}
