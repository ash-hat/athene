using System;
using System.Collections;
using System.Collections.Generic;

namespace Athene.Extensions
{
	internal readonly struct NonNullEnumerator : IEnumerator
	{
		private readonly IEnumerator _enumerator;

		public object Current => _enumerator.Current ?? throw new InvalidOperationException("Underlying enumerator returned a null value.");

		public NonNullEnumerator(IEnumerator enumerator)
		{
			_enumerator = enumerator ?? throw new ArgumentNullException(nameof(enumerator));
		}

		public bool MoveNext()
		{
			return _enumerator.MoveNext();
		}

		public void Reset()
		{
			_enumerator.Reset();
		}
	}

	internal readonly struct NonNullEnumerator<TValue> : IEnumerator<TValue> where TValue : class
	{
		private readonly IEnumerator<TValue> _enumerator;

		public TValue Current => _enumerator.Current ?? throw new InvalidOperationException("Underlying enumerator returned a null value.");

		object IEnumerator.Current => ((IEnumerator) _enumerator).Current ?? throw new InvalidOperationException("Underlying enumerator returned a null value.");

		public NonNullEnumerator(IEnumerator<TValue> enumerator)
		{
			_enumerator = enumerator;
		}

		public void Dispose()
		{
			_enumerator.Dispose();
		}

		public bool MoveNext()
		{
			return _enumerator.MoveNext();
		}

		public void Reset()
		{
			_enumerator.Reset();
		}
	}
}
