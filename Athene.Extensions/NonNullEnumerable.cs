using System.Collections;
using System.Collections.Generic;

namespace Athene.Extensions
{
	internal readonly struct NonNullEnumerable : IEnumerable
	{
		private readonly IEnumerable _enumerable;

		public NonNullEnumerable(IEnumerable enumerable)
		{
			_enumerable = enumerable;
		}

		public IEnumerator GetEnumerator()
		{
			return new NonNullEnumerator(_enumerable.GetEnumerator());
		}
	}

	internal readonly struct NonNullEnumerable<TValue> : IEnumerable<TValue> where TValue : class
	{
		private readonly IEnumerable<TValue> _enumerable;

		public NonNullEnumerable(IEnumerable<TValue> enumerable)
		{
			_enumerable = enumerable;
		}

		public IEnumerator<TValue> GetEnumerator()
		{
			return new NonNullEnumerator<TValue>(_enumerable.GetEnumerator());
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return new NonNullEnumerator(((IEnumerable) _enumerable).GetEnumerator());
		}
	}
}
