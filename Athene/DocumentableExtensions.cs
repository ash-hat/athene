using System;

namespace Athene
{
	/// <summary>
	/// 	Extensions for <see cref="IDocumentable"/>.
	/// </summary>
	public static class DocumentableExtensions
	{
		/// <summary>
		///     Formats a documentable into a page.
		/// </summary>
		/// <param name="documentable">The documentable whose information is to be displayed.</param>
		/// <param name="alias">The name used to execute the command.</param>
		/// <returns>A help page for the command.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="documentable" /> or <paramref name="alias" /> is <see langword="null"/>.</exception>
		public static string GetHelpPage(this IDocumentable documentable, string alias)
		{
			if (documentable == null)
			{
				throw new ArgumentNullException(nameof(documentable));
			}

			if (alias == null)
			{
				throw new ArgumentNullException(nameof(alias));
			}

			string formatted = alias;
			string signature = documentable.Signature;

			if (!string.IsNullOrEmpty(signature))
			{
				formatted += " " + signature;
			}

			return formatted + Environment.NewLine + documentable.Description;
		}
	}
}
