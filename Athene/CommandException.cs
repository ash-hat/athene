﻿using System;

namespace Athene
{
	/// <summary>
	///     An error that occurs during command execution and is to be displayed to the user.
	/// </summary>
	public class CommandException : Exception
	{
		/// <inheritdoc />
		public CommandException(string message) : base(message)
		{
		}

		/// <inheritdoc />
		public CommandException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}
