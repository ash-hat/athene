using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A delimited closure that uses the raw value of the character grouping as the token value.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public sealed class RawDelimitedClosure<TContext> : EnumerableDelimitedClosure<TContext> where TContext : IExecutionContext
	{
		private bool _interpolate;

		/// <summary>
		/// 	Whether or not the character grouping is able to be interpolated.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool Interpolate
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(RawDelimitedClosure<TContext>));
				}

				return _interpolate;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(RawDelimitedClosure<TContext>));
				}

				_interpolate = value;
			}
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="RawDelimitedClosure{TContext}"/>.
		/// </summary>
		/// <param name="delimiter">The delimiting characters between tokens.</param>
		/// <exception cref="ArgumentNullException"><paramref name="delimiter"/> is <see langword="null"/>.</exception>
		public RawDelimitedClosure(IEnumerable<char> delimiter) : base(delimiter ?? throw new ArgumentNullException(nameof(delimiter)))
		{
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="RawDelimitedClosure{TContext}"/>.
		/// </summary>
		~RawDelimitedClosure()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="EnumerableDelimitedClosure{TContext}.Process"/>
		/// <exception cref="ArgumentNullException"><paramref name="token"/> is <see langword="null"/>.</exception>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public override Token Process(IEnumerable<char> token, TContext context)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(RawDelimitedClosure<TContext>));
			}

			if (token == null)
			{
				throw new ArgumentNullException(nameof(token));
			}

			return new Token(token, Interpolate);
		}
	}
}
