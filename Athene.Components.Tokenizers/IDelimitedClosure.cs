namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A grouper for token characters, grouped by a delimiter.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface IDelimitedClosure<in TContext> : ITokenClosure<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Advances the matching of the delimiter.
		/// </summary>
		/// <param name="c">The current character of the tokenizable characters.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <param name="consumed">The characters consumed when advancing the closure.</param>
		/// <returns>Whether or not it is a complete match of the delimiter.</returns>
		bool AdvanceDelimiter(char c, TContext context, out int consumed);
	}
}
