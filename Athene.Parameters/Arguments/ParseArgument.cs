﻿using System;

namespace Athene.Parameters.Arguments
{
	/// <summary>
	///     An argument that's value is parsed from its own token.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	/// <typeparam name="TValue">The type of the parsed argument.</typeparam>
	public class ParseArgument<TContext, TValue> : BaseArgument<TContext, TValue> where TContext : IExecutionContext
	{
		private readonly ContextualTryParseHandler<TContext, TValue> _tryParseHandler;

		/// <summary>
		///     Constructs an instance of <see cref="ParseArgument{TContext, TValue}" />.
		/// </summary>
		/// <param name="name">Name of the argument.</param>
		/// <param name="valueName">Name of the value's type.</param>
		/// <param name="description">Detailed explanation of the argument.</param>
		/// <param name="tryParseHandler">Parse function to convert a token into an instance of <typeparamref name="TValue" />.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name" />, <paramref name="valueName" />, or <paramref name="description" /> is <see langword="null"/>.</exception>
		public ParseArgument(string name, string valueName, string description, ContextualTryParseHandler<TContext, TValue> tryParseHandler) : base(name, valueName, description)
		{
			_tryParseHandler = tryParseHandler;
		}

		/// <inheritdoc />
		protected override TValue ProcessValue(string token, TContext context)
		{
			if (!_tryParseHandler(token, context, out TValue value))
			{
				throw new CommandException("Failed to convert.");
			}

			return value;
		}
	}

	/// <summary>
	///     An argument that's value is parsed from its own token.
	/// </summary>
	/// <typeparam name="TValue">The type of the parsed argument.</typeparam>
	public sealed class ParseArgument<TValue> : ParseArgument<IExecutionContext, TValue>
	{
		/// <summary>
		///     Constructs an instance of <see cref="ParseArgument{TContext, TValue}" />.
		/// </summary>
		/// <param name="name">Name of the argument.</param>
		/// <param name="valueName">Name of the value's type.</param>
		/// <param name="description">Detailed explanation of the argument.</param>
		/// <param name="tryParseHandler">Parse function to convert a token into an instance of <typeparamref name="TValue" />.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name" />, <paramref name="valueName" />, or <paramref name="description" /> is <see langword="null"/>.</exception>
		public ParseArgument(string name, string valueName, string description, TryParseHandler<TValue> tryParseHandler) : base(name, valueName, description, (string raw, IExecutionContext _, out TValue result) => tryParseHandler(raw, out result))
		{
		}
	}
}
