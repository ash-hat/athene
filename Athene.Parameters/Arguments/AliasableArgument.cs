﻿using System;
using Athene.Tables;

namespace Athene.Parameters.Arguments
{
	/// <summary>
	///     An argument that resolves to a command.
	/// </summary>
	public class AliasableArgument<TContext, TValue> : BaseArgument<TContext, TValue> where TContext : IExecutionContext
	{
		private readonly IAliasTable<TValue> _aliasTable;

		/// <summary>
		///     The raw alias of the value provided.
		/// </summary>
		public string Alias { get; private set; }

		/// <summary>
		///     Constructs an instance of <see cref="AliasableArgument{TContext,TValue}" />.
		/// </summary>
		/// <param name="aliasTable">The aliases and associated values that can be used.</param>
		/// <param name="name">Name of this argument.</param>
		/// <param name="valueName">Name of this argument's value.</param>
		/// <param name="description">Detailed purpose of this argument.</param>
		/// <exception cref="ArgumentNullException">
		///     <paramref name="aliasTable" />, <paramref name="name" />, <paramref name="valueName" />, or <paramref name="description" /> is <see langword="null"/>.
		/// </exception>
		public AliasableArgument(IAliasTable<TValue> aliasTable, string name, string valueName, string description) : base(name ?? throw new ArgumentNullException(nameof(name)),
			valueName ?? throw new ArgumentNullException(nameof(valueName)), description ?? throw new ArgumentNullException(nameof(description)))
		{
			_aliasTable = aliasTable ?? throw new ArgumentNullException(nameof(aliasTable));
		}

		/// <inheritdoc cref="BaseArgument{TContext,TValue}.ProcessValue" />
		protected override TValue ProcessValue(string token, TContext context)
		{
			TValue value = _aliasTable[token];
			if (value == null)
			{
				throw new CommandException("Aliasable not found.");
			}

			Alias = token;
			return value;
		}

		/// <inheritdoc cref="BaseArgument{TContext,TValue}.Clear" />
		public override void Clear()
		{
			base.Clear();

			Alias = null;
		}
	}

	/// <summary>
	///     An argument that resolves to a command, without using context.
	/// </summary>
	public sealed class AliasableArgument<TValue> : AliasableArgument<IExecutionContext, TValue>
	{
		/// <inheritdoc cref="AliasableArgument{TContext,TValue}(Athene.Tables.IAliasTable{TValue},string,string,string)"/>
		public AliasableArgument(IAliasTable<TValue> aliasTable, string name, string valueName, string description) : base(aliasTable, name, valueName, description)
		{
		}
	}
}
