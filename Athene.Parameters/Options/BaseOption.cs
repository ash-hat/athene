﻿using System;
using System.Collections.Generic;

namespace Athene.Parameters.Options
{
	/// <inheritdoc cref="IOption"/>
	public abstract class BaseOption<TContext> : IParameter<TContext>, IOption where TContext : IExecutionContext
	{
		/// <inheritdoc />
		public string LongName { get; }

		/// <inheritdoc />
		public char? ShortName { get; }

		/// <inheritdoc />
		public string Signature { get; }
		/// <inheritdoc />
		public string Description { get; }

		/// <inheritdoc />
		public bool Required { get; } = false;
		/// <inheritdoc />
		public bool HasValue { get; private set; }

		/// <summary>
		///     Constructs an instance of <see cref="BaseOption{TContext}" />.
		/// </summary>
		/// <param name="shortName">The short, single-character representation of the option.</param>
		/// <param name="longName">The full, multi-character representation of the option.</param>
		/// <param name="description">Detailed information about the purpose and usage of this instance.</param>
		/// <param name="signature">Compact information about the identification or purpose as well as usage of this instance.</param>
		/// <exception cref="ArgumentNullException"><paramref name="description" /> or <paramref name="signature" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are <see langword="null"/>. Only one may be <see langword="null"/>.</exception>
		protected BaseOption(char? shortName, string longName, string description, Func<string> signature)
		{
			if (signature == null)
			{
				throw new ArgumentNullException(nameof(signature));
			}

			if (shortName == null && longName == null)
			{
				throw new ArgumentException("A short name or long name must be provided (both were null).");
			}

			Signature = signature();
			Description = description ?? throw new ArgumentNullException(nameof(description));

			ShortName = shortName;
			LongName = longName;
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException">A value of <paramref name="tokens"/> is <see langword="null"/>.</exception>
		public bool Update(IEnumerator<string> tokens, TContext context)
		{
			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			string token = tokens.Current ?? throw new ArgumentException($"Token is null.", nameof(tokens));

			// The token is not long enough to be an option or is not prefixed.
			if (token.Length <= 1 || token[0] != '-')
			{
				return false;
			}

			if (token[1] == '-') // --
			{
				// No long name exists or the token does not match the long name.
				if (LongName == null || token.Substring(2) != LongName)
				{
					return false;
				}
			}
			else // -
			{
				// Prefixed with short name but no short name exists.
				if (!ShortName.HasValue)
				{
					return false;
				}

				// Token has more than one character after prefix or the character does not match the short name.
				string name = token.Substring(1);
				if (name.Length != 1 || name[0] != ShortName.Value)
				{
					return false;
				}
			}

			UpdateValue(tokens, context);
			HasValue = true;

			return true;
		}

		/// <inheritdoc />
		public virtual void Clear()
		{
			HasValue = false;
		}

		/// <summary>
		///     Updates the value of this option, incrementing the index only by as many tokens read.
		/// </summary>
		/// <param name="tokens">The remaining tokens of the command</param>
		/// <param name="context">The currently running execution context.</param>
		protected abstract void UpdateValue(IEnumerator<string> tokens, TContext context);

		/// <inheritdoc />
		public override string ToString()
		{
			return Signature;
		}
	}
}
