﻿namespace Athene.Parameters
{
	/// <summary>
	///     A delegate that converts a <see langword="string" /> into a <code>TValue</code>. This pattern matches most primitives' <code>TryParse(string s, out TPrimitive result)</code>
	///     methods.
	/// </summary>
	/// <param name="raw"><see langword="string" /> representation of the value.</param>
	/// <param name="context">The currently running execution context.</param>
	/// <param name="result">The parsed value if succeeded. Otherwise it is <see langword="default"/>.</param>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	/// <typeparam name="TValue">Type of the parsed value.</typeparam>
	/// <returns>Whether or not parsing was successful.</returns>
	public delegate bool ContextualTryParseHandler<in TContext, TValue>(string raw, TContext context, out TValue result) where TContext : IExecutionContext;
}
