﻿namespace Athene.Parameters
{
	/// <summary>
	///     A delegate that converts a <see langword="string" /> into a <code>TValue</code>. This pattern matches most primitives' <code>TryParse(string s, out TPrimitive result)</code>
	///     methods.
	/// </summary>
	/// <param name="raw"><see langword="string" /> representation of the value.</param>
	/// <param name="result">The parsed value if succeeded. Otherwise it is <see langword="default"/>.</param>
	/// <typeparam name="TValue">Type of the parsed value.</typeparam>
	/// <returns>Whether or not parsing was successful.</returns>
	public delegate bool TryParseHandler<TValue>(string raw, out TValue result);
}
